angular.module('cloudhealth', [
    'mobile-angular-ui',
    'cloudhealth.controllers',
    'cloudhealth.services',
    'ngRoute',
    'ngDialog',
    'angularSpinner',
    'uiGmapgoogle-maps',
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when("/location", {
        templateUrl: "locations.html",
        controller: "locationControl",
    })
    .when("/vaccines/:infantid", {
        templateUrl: "vaccines.html",
        controller: "vaccinesControl",
    })
    .when("/ailments/:infantid", {
        templateUrl: "ailments.html",
        controller: "ailmentsControl",
    })
    .when("/recommendations/:infantid", {
        templateUrl: "recommendations.html",
        controller: "recommendationsControl",
    })
    .when("/childprofile/:infantid", {
        templateUrl: "childprofile.html",
        controller: "childprofileControl",
    })
    .when("/profile", {
        templateUrl: "profile.html",
        controller: "profileControl",
    })
    .when("/results/:infantid", {
        templateUrl: "results.html",
        controller: "resultsControl",
    })
    .when("/options/:infantid", {
        templateUrl: "options.html",
        controller: "optionsControl",
    })
    .when("/home", {
        templateUrl: "home.html",
        controller: "homeControl",
    })
    .when("/signup", {
        templateUrl: "signup.html",
        controller: "signupControl",
    })
    .when("/newinfant", {
        templateUrl: "newinfant.html",
        controller: "newinfantControl",
    })
    .when("/", {
        templateUrl: "signin.html",
        controller: "loginControl",
    })
    .otherwise({ redirectTo: '/' });

}])

.run([function() {

  $( document ).on( "deviceready", function(){

    //setting the status bar margin and color
    StatusBar.overlaysWebView( false );
    StatusBar.styleDefault();
    StatusBar.backgroundColorByHexString("#F7F7F7");

    //set different notification bar background for android
    if (device.platform == 'Android') {
      StatusBar.backgroundColorByHexString("#333");
    }

/*    ga_storage._setAccount('UA-75823862-3'); //Replace with your own
    ga_storage._trackPageview('/index.html', 'Home');
*/
    //setting viewport for windows
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(
        document.createTextNode(
          "@-ms-viewport{width:auto!important}"
        )
      );
      document.getElementsByTagName("head")[0].
        appendChild(msViewportStyle);
    }
  });
}]);
