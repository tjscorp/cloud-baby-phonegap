angular.module('cloudhealth.controllers', [])

.controller('locationControl', function($scope, $location, $rootScope, locationService, global) {

  global.updateNavBar(true, $rootScope, "Find a Centre");

  locationService.maplocations($scope);
  $scope.map = { center: { latitude: -17.8333, longitude: 31.0500 }, zoom: 5 };

})

.controller('optionsControl', function($scope, $location, $rootScope, avService, $routeParams, global) {
  global.updateNavBar(true, $rootScope, "Options");

  $scope.infantid = $routeParams.infantid;

})

.controller('vaccinesControl', function($scope, $location, $rootScope, avService, $routeParams, global) {
  global.updateNavBar(true, $rootScope, "Vaccinations");

  avService.vaccines($scope, $routeParams.infantid);

})

.controller('ailmentsControl', function($scope, $location, $rootScope, avService, $routeParams, global) {
  global.updateNavBar(true, $rootScope, "Ailments");

  //log($routeParams.infantid);
  avService.ailments($scope, $routeParams.infantid);

})


.controller('recommendationsControl', function($scope, $location, $rootScope, $routeParams, recommendationsService, global) {
  global.updateNavBar(true, $rootScope, "Recommendations");

  recommendationsService.getRecos($scope);

})

.controller('childprofileControl', function($scope, $location, $anchorScroll, $rootScope, $routeParams, infantService, loginService, global) {
  global.updateNavBar(true, $rootScope, "Child's Details");

  $scope.updateChildProfile = function(){
    infantService.updateChildProfile($scope, $routeParams.infantid);
  };

  infantService.getChildProfile($scope, localStorage.username, $routeParams.infantid);

})

.controller('profileControl', function($scope, $location, $anchorScroll, $rootScope, homeService, loginService, global) {
  global.updateNavBar(true, $rootScope, "Profile Details");

  $scope.scroll = function (hash) {
    // set the location.hash to the id of the element you wish to scroll to.
    $location.hash(hash).replace();
    $anchorScroll();
  };

  $scope.updateProfile = function(){
    homeService.updateProfile($scope);
  };

  homeService.getProfile($scope, localStorage.username);

})

.controller('resultsControl', function($scope, $location, $rootScope, $routeParams, homeService, loginService, global) {

  if( !loginService.checkLogin() ) $location.path("/");
  global.updateNavBar(true, $rootScope, "Baby Results");

  homeService.getCharts($scope, $routeParams.infantid);

})

.controller('newinfantControl', function($scope, $location, $rootScope, infantService, loginService, global) {

  global.updateNavBar(true, $rootScope, "Register a Baby / Pregnancy");

  $scope.newinfant = function() {
    infantService.newinfant($scope, localStorage.username);
  }

  $scope.checktype= function() {
    if( $("#type").val() == "pregnancy" ) $scope.isbaby = false;
    else if( $("#type").val() == "baby" ) $scope.isbaby = true;
  }

  $scope.type = "0";

})

.controller('homeControl', function($scope, $location, $rootScope, ngDialog, homeService, loginService, global) {

  if( !loginService.checkLogin() ) $location.path("/");
  global.updateNavBar(false, $rootScope, "Cloud Baby");
  //log(localStorage);
  $scope.addChild = function() {
    if(
      localStorage.nationalid == "" ||
      localStorage.email == "" ||
      localStorage.homeaddress == ""
    ) {
      //log(1);
      ngDialog.open({
          template: 'completeRegistration',
          controller: 'completeRegistrationControl',
      });
    }else $location.path("/newinfant");
  }


  try { homeService.setPush();/*push notifications initializer*/ }
  catch (e) { log(e); }

  homeService.getInfants($scope, $rootScope, localStorage.username);

})

.controller('completeRegistrationControl', function($scope, $location, $rootScope, loginService) {


})

.controller('signupControl', function($scope, $location, $rootScope, loginService) {

    $scope.signup = function() {
        loginService.signup();
    }

})

.controller('loginControl', function($scope, $location, $rootScope, loginService) {
  if( loginService.checkLogin() ) {
      $rootScope.$navbartop = true;
      $rootScope.$navbarbottom = true;
      $location.path("/home");
  }else if( !loginService.checkLogin() ) {
      $rootScope.$navbartop = false;
      $rootScope.$navbarbottom = false;
      loginService.logout($scope);
  }

  $scope.login = function() {
      loginService.login($scope.username, $scope.password);
  }
})

.controller('myapp', function($scope, loginService) {

  $scope.logout = function() {
      loginService.logout($scope);
  };

  $scope.$back = function() {
      window.history.back();
  }
});
