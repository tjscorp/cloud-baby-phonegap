angular.module('cloudhealth.services', [])

.factory('recommendationsService', function($http, $location, global, $rootScope){
  var rs = {};

  rs.getRecos = function($scope) {
    global.startSpin();

    $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p=_&action=recommendations",
        params: { username: localStorage.username }
    })
    .then(function(response) {
        log(response.data);
        var data = response.data;

        for (var r in data) data[r].send_time = moment(data[r].send_time).format('MMMM Do YYYY, h:mm:ss a');
        $scope.recos = data;

        global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  return rs;
})

.factory('infantService', function($http, $location, global, $rootScope){
  var is = {};

  is.newinfant = function($scope, username) {
    global.startSpin();

    var d = $("#newinfant").serializeObject();
    d.parentid = username;

    $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p=_&action=newinfant",
        params: d
    })
    .then(function(response) {
        log(response);
        var data= response.data;
        if( data.ok ) {
          $location.path("/home");
          //$scope.vaccines = response.data;
          //log(response.data);
        }else {
          log("fail");
          $(".help-block").hide();
          var e = data.message;
          for (var a in e) {
              $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().text(e[a].message);
          };
        };
        global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  is.updateChildProfile = function($scope, infantid) {
    global.startSpin();
    var form = $("#signupform").serializeObject();
    form.infantid = infantid;

    log(form);
    $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p=_&action=newinfant",
        params: form
    })
    .then(function(response) {
        log(response);
        var data = response.data;
        if( data.ok ) {
          //clear error messages
          $(".help-block").hide();
          $(".has-error").removeClass("has-error");
          $(function () { $scope.updatesuccess = true; })
        }else {
            log("fail");
            $(".help-block").hide();
            $(".has-error").removeClass("has-error");
            var e = data.message;
            for (var a in e) {
                $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().html(e[a].message);
            };
        };
        global.stopSpin();
      }, function(response) {
        log(response);
        global.stopSpin();
      });
  }

  is.getChildProfile = function($scope, username, infantid){
    global.startSpin();
    $http({
      method: 'JSONP',
      timeout: global.timeout,
      url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=getChildProfile',
      params: {"username": localStorage.username, "infantid": infantid}
    })
    .then(function(response) {
      log(response);
      var data = response.data;
      for( var i in data ) {
        $scope[i] = data[i];
      }
      //set datepicker
      $( "#dob" ).datepicker({
        dateFormat: "D M d yy"
      });
      global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  return is;
})

.factory('avService', function($http, $location, global, $rootScope){
  var av = {};

  av.vaccines = function($scope, infantid) {
    global.startSpin();
    $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p=_&action=vaccines",
        params: {"username": localStorage.username, "infantid": infantid}
    })
    .then(function(response) {
        log(response);

        $scope.vaccines = response.data;
        //log(response.data);

        global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  av.ailments = function($scope, infantid) {
    global.startSpin();
    $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p=_&action=ailments",
        params: {"username": localStorage.username, "infantid": infantid}
    })
    .then(function(response) {
        log(response);

        $scope.ailments = response.data;
        //log(response.data);

        global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  return av;
})

.factory('homeService', function($http, $location, global, $rootScope){
  var hs = {};

  hs.updateProfile = function($scope) {
    global.startSpin();
    var form = $("#signupform").serializeObject();
    form.clientid = localStorage.username;

    log(form);
    $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p=_&action=signup",
        params: form
    })
    .then(function(response) {
        log(response);
        var data = response.data;
        if( data.ok ) {
          //clear error messages
          $(".help-block").hide();
          $(".has-error").removeClass("has-error");
          $(function () { $scope.updatesuccess = true; })
        }else {
            log("fail");
            $(".help-block").hide();
            $(".has-error").removeClass("has-error");
            var e = data.message;
            for (var a in e) {
                $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().html(e[a].message);
            };
        };
        global.stopSpin();
      }, function(response) {
        log(response);
        global.stopSpin();
      });
  }

  hs.getProfile = function($scope, username){
    global.startSpin();
    $http({
      method: 'JSONP',
      timeout: global.timeout,
      url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=getProfile',
      params: { username: username }
    })
    .then(function(response) {
      log(response);
      var data = response.data;
      for( var i in data ) {
        $scope[i] = data[i];
      }
      //set datepicker
      $( "#dob" ).datepicker({
        dateFormat: "D M d yy"
      });
      global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  hs.getInfants = function($scope, $root, username){
    global.startSpin();
    $http({
      method: 'JSONP',
      timeout: global.timeout,
      url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=getInfants',
      params: { username: username }
    })
    .then(function(response) {
      log(response);
      var data = response.data;

      $scope.infants = $root.$infants = response.data;

      global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });
  }

  hs.setPush = function() {

    //set push notifications mechanism
    var push = PushNotification.init({
        android: {
          senderID: "342563283322"
        },
        ios: {
          alert: "true",
          badge: true,
          sound: 'true',
          clearBadge: true
        },
        windows: {}
    });

    push.on('registration', function(data) {
      // data.registrationId
      log(data);
      //alert(data.registrationId+":"+device.platform);
      $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=notify',
        params: {	regid:data.registrationId, platform: device.platform, username: localStorage.username },
      })
      .then(function(response) {
        log(response);
        global.stopSpin();
      }, function(response) {
        log(response);
        global.stopSpin();
      });
    });

    push.on('notification', function(data) {
      // data.message,
      // data.title,
      // data.count,
      // data.sound,
      // data.image,
      // data.additionalData
      //alert(data);

      //clear badge
      data.count = 0;
      if( data.additionalData.coldstart ) {
        $location.path("/results");
      }
    });

    push.on('error', function(e) {
        // e.message
        $rootScope.$network = false;
        alert("Problem registering you device for notifications, make sure your internet connectionion is\
        active and then restart the application");
    });

  }

  hs.getCharts = function($scope, infantid){
    global.startSpin();
    $http({
      method: 'JSONP',
      timeout: global.timeout,
      url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=getCharts',
      params: { "username": localStorage.username, "infantid":infantid }
    })
    .then(function(response) {
      log(response);

      $scope.name = response.data.name;

      Chart.types.Line.extend({
          name: "LineAlt",
          draw: function () {
              Chart.types.Line.prototype.draw.apply(this, arguments);

              var ctx = this.chart.ctx;
              ctx.save();
              // text alignment and color
              ctx.textAlign = "center";
              ctx.textBaseline = "bottom";
              ctx.fillStyle = this.options.scaleFontColor;
              // position
              var x = this.scale.xScalePaddingLeft * 0.4;
              var y = this.chart.height / 2;
              // change origin
              ctx.translate(x, y)
              // rotate text
              ctx.rotate(-90 * Math.PI / 180);
              ctx.fillText(this.datasets[0].label, 0, 0);
              ctx.restore();
          }
      });

      var headchartdata = {
        labels: response.data.headxaxis,
        datasets: [
          {
            label: "Head Measurements",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "#C8B193",
            pointColor: "#C8B193",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#C8B193",
            data: response.data.headyaxis
          }
        ]
      }

      var ctx = $("#headChart").get(0).getContext("2d");
      var myLineChart = new Chart(ctx).LineAlt(headchartdata, {
        responsive: true,
        scaleLabel: "      <%=value%>",
        multiTooltipTemplate: "<%= datasetLabel %> - <%= value %> kg"
      });
      //var legend = myLineChart.generateLegend();
      //$("#headlegend").append(legend);

      var weightchartdata = {
        labels: response.data.weightxaxis,
        datasets: [
          {
            label: "Weight (kg)",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "#C8B193",
            pointColor: "#C8B193",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#C8B193",
            data: response.data.weightyaxis
          },
          {
            label: "Average Growth Rate",
            fillColor: "rgba(200,200,200,0.1)",
            strokeColor: "#FA705D",
            pointColor: "#FA705D",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#FA705D",
            data: response.data.agryaxis
          }
        ]
      }

      var ctx = $("#weightChart").get(0).getContext("2d");
      var myLineChart = new Chart(ctx).LineAlt(weightchartdata, {
        responsive: true,
        scaleLabel: "      <%=value%>",
        multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
      });

      var heightchartdata = {
        labels: response.data.heightxaxis,
        datasets: [
          {
            label: "Height",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "#C8B193",
            pointColor: "#C8B193",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#C8B193",
            data: response.data.heightyaxis
          }
        ]
      }

      var ctx = $("#heightChart").get(0).getContext("2d");
      var myLineChart = new Chart(ctx).LineAlt(heightchartdata, {
        responsive: true,
        scaleLabel: "      <%=value%>",
        multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
      });

      global.stopSpin();
    }, function(response) {
      log(response);
      global.stopSpin();
    });

  }

  return hs;
})

.factory('loginService', function($http, $location, global, $rootScope){
    var lc = {};

    lc.signup = function() {
        var su = $("#signupform").serializeObject();
        //log(su);
        //clear error messages
        $(".help-block").hide();
        $(".has-error").removeClass("has-error");
        global.startSpin();

        $http({
            method: 'JSONP',
            url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=signup',
            params: su,
        })
        .then(function(response) {
            log(response);
            data = response.data;

            if(data.ok) {
              log("success");
              localStorage.username = data._id;
              localStorage.email = data.email;
              localStorage.nationalid = data.nationalid;
              localStorage.homeaddress = data.homeaddress;
              location.href = "index.html#/home";
            }else {
              log("fail");
              $(".help-block").hide();
              var e = data.message;
              for (var a in e) {
                  $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().html(e[a].message);
              };
            };
            global.stopSpin();
        });
    }

    lc.login = function(username, password){
      global.startSpin();
      $http({
        method: 'JSONP',
        timeout: global.timeout,
        url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=signin',
        params: { "username": username, "password": password },
      })
      .then(function(response) {
        log(response);
        data = response.data;
        if(data.ok) {
          //clear error messages
          $(".help-block").hide();
          $(".has-error").removeClass("has-error");

          log("success");
          localStorage.username = data._id;
          localStorage.email = data.email;
          localStorage.nationalid = data.nationalid;
          localStorage.homeaddress = data.homeaddress;
          location.href = "index.html#/home";
        }else {
          log("fail");
          $(".help-block").hide();
          var e = data.message;
          for (var a in e) {
              $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().text(e[a].message);
          };
        };
          global.stopSpin();
      }, function(response) {
        log(response);
        global.stopSpin();
      });
  }

  lc.checkLogin = function() {
    if( typeof(localStorage.username) == "undefined" || typeof(localStorage.username) == "null" ) return false;
    else if( typeof(localStorage.username) != "undefined" && typeof(localStorage.username) != "null" ) return true
  }

  lc.logout = function($scope) {
    delete localStorage.username;
    $rootScope.$navbartop = false;
    $rootScope.$navbarbottom = false;

    $location.path("/");
  }

  return lc;
})

.factory('global', function(usSpinnerService){
  return {
    host : 'cloudhealthtechnology.com/cloudhealth',
    //host : 'sellingpoint.co.zw/cloudhealth',
    //host : 'localhost/cloudhealth',
    timeout: 10000,
    startSpin: function() {
      usSpinnerService.spin('mySpinner');
    },
    stopSpin: function() {
      usSpinnerService.stop('mySpinner');
    },
    updateNavBar: function(back, scope, brand) {
      scope.$needsBack = back;
      scope.$brand = brand;
      scope.$navbartop = true;
      //scope.$supplier = true;
      //scope.$navbarbottom = true;
      switch(brand){
        case "":
          scope.$logo = true;
        break;
      }
    }
  }
});
